import asyncio
import json
import websockets

MESSAGE_TYPES=['get_user_list','get_online_user_list'
        ,'message','status']

online_clients={}
all_clients=[('user1',1),('user2',2),('user3',3),('user4',4)]


@asyncio.coroutine
def get_user_list(message,websocket):
    #res = "[{'id':1,'name':'name 1','avatar':'','status':'online'},{'id':2,'name':'name 2','avatar':'','status':'offline'}]"
    rem = list(online_clients.keys())
    temp = list(map(lambda x:{'id':x[1],'name':x[0],'avatar':'',\
            'status':'online' if x in rem else 'offline'},all_clients))
    yield from websocket.send(json.dumps(temp))
    print(temp)

@asyncio.coroutine
def get_online_user_list(message,websocket):
    #res = "[{'id':1,'name':'name 1','avatar':'','status':'online'},{'id':2,'name':'name 2','avatar':'','status':'online'}]"
    rem = online_clients.keys()
    temp = list(map(lambda x:{'id':x[1],'name':x[0],'avatar':'','status':'online'},rem))
    yield from websocket.send(json.dumps(temp))
    print(temp)

@asyncio.coroutine
def message(message,websocket):
    #res = "{'type':'message','message':'Hello','from':2}"
    dst = list(filter(lambda x: x[1]==message['to'],online_clients.keys()))[0]
    rem = list(filter(lambda x: x[1]==websocket,online_clients.items()))[0]
    temp ={'from':rem[0][1],'message':message['message'],'type':'message'}
    yield from online_clients[dst].send(json.dumps(temp))
    #yield from websocket.send(res)
    print(temp)

@asyncio.coroutine
def status(message,websocket):
    #res = "{'type':'status','id':2,'status':'online'}"
    yield from websocket.send(json.dumps(message))


def persist_message(message):
    with open('log.txt','a') as file:
        file.write(json.dumps(message))

MESSAGE_MAP={0:get_user_list,1:get_online_user_list,2:message,3:status}

@asyncio.coroutine
def hello(websocket, path):
    junk,username,userid = path.split('/')
    userid = int(userid)
    print((username,userid))
    for cl in online_clients.values():
        res = {'type':'status','id':userid,'status':'online'}
        yield from MESSAGE_MAP[3](res,cl)

    online_clients[(username,userid)]=websocket
    while True:
        name =None
        try:
            name = yield from websocket.recv()
        except Exception as e:

            del online_clients[(username,userid)]
            for cl in online_clients.values():
                res = {'type':'status','id':userid,'status':'offline'}
                yield from MESSAGE_MAP[3](res,cl)
            break

        print("< {}".format(name))
        res = {}
        try:
            res = json.loads(name)
        except Exception as e:
            continue
        if 'type' in res.keys():
            message_type = MESSAGE_TYPES.index(res['type'])
            if message_type == 2:
                res['from']=userid
                persist_message(res)

            yield from MESSAGE_MAP[message_type](res,websocket)
            



    
start_server = websockets.serve(hello, 'localhost', 8765)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()

